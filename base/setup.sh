#!/bin/bash
#
# source this file to prep your ci job environment

[[ -n "$BASE_DIR" ]] && cd "$BASE_DIR"

if [[ -f dist.ini ]] ; then
    echo "dist.ini found!"
    DIST="${DIST:-$(perl -nE 'say $+{dist} if /^name\s*=\s*(?<dist>.*)$/' dist.ini)}"
elif [[ -f "$CI_BUILDS_DIR/$CI_PROJECT_DIR/dist" ]] ; then
    echo "dist found at $CI_BUILDS_DIR/$CI_PROJECT_DIR/dist"
    DIST="$(cat "$CI_BUILDS_DIR/$CI_PROJECT_DIR/dist")"
fi

git config --global user.name  "$GITLAB_USER_NAME"
git config --global user.email "$GITLAB_USER_EMAIL"
