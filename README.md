# Perl CI images

Base images for use in building and testing CPAN-style distributions,
including reporting detailed test results to GitLab (for use in Merge
Requests).

See, e.g.

* https://gitlab.com/rsrchboy/moosex-attributeshortcuts

<!-- vim-markdown-toc GitLab -->

* [Quickstart!](#quickstart)
* [CI components provided](#ci-components-provided)
* [Image composition](#image-composition)
    * [`base` image](#base-image)
    * [`dzil` image](#dzil-image)
* [Pipeline configuration](#pipeline-configuration)
    * [Stages](#stages)
    * [Artifacts](#artifacts)
* [Pipeline configuration of _this_ repository](#pipeline-configuration-of-_this_-repository)

<!-- vim-markdown-toc -->

# Quickstart!

Create a `.gitlab-ci.yml` in your project that reads something like:

```yaml
include: https://gitlab.com/rsrchboy/perl-ci/raw/master/dzil-ci.yml
```

Test results are fed back to GitLab as an [`artifacts:reports:junit`
report](https://docs.gitlab.com/ee/ci/yaml/#artifactsreportsjunit).  These
results are then used by GitLab to show test pass/fail deltas in Merge
Requests and the like.

# CI components provided

CI testing / packaging of `Dist::Zilla` based distributions is fairly
straight-forward.  This project provides a couple bits of common code to make
this easier:

* Docker images for distribution building and testing

    * `gitlab.com/rsrchboy/perl-ci/dzil` for operations requiring
        Dist::Zilla, including the ever popular `dzil build`
    * `gitlab.com/rsrchboy/perl-ci` for operations not requiring `Dist::Zilla`
        support

    Note that we have pre-installed a bunch of commonly used modules in these
    images, and use `cpanm` to install anything additional required.

* Generic pipeline configuration for `build`, `test`

    Rather than maintaining the same pipeline config in every project, we can
    simply include `dzil-ci.yml`, generally resulting in one-line
    `.gitlab-ci.yml` configurations:

    ```yaml
    include: https://gitlab.com/rsrchboy/perl-ci/raw/master/dzil-ci.yml
    ```
# Image composition

## `base` image

`gitlab.com/rsrchboy/perl-ci:latest`

The `base` image is based off of `alpine:latest`.  It installs many CPAN dists
via `apk`, the native alpine package manager, both for speed and ease of
installing XS modules.  (We also install everything necessary to build and
install off of the CPAN, including `gcc` and `cpanm`.)

We also include a setup script, `/setup.sh`, suitable for sourcing in a CI
`before_script` scriptlet.  Among other things, this script figures out the
distribution we're working with and sets `git`'s `core.{name,email}` to the
name / email of the GitLab user who owns this pipeline run.

We use this image in our standard `test` job.

## `dzil` image

`gitlab.com/rsrchboy/perl-ci/dzil:latest`

This image is based off of our `base` image, and additionally includes a slew
of `Dist::Zilla` and plugins.  It is a good starting point for any job
requiring `dzil`, e.g. our standard `build` job.

# Pipeline configuration

## Stages

We use:

* `build`, essentially `dzil build`; and
* `test`, essentially `make test`

## Artifacts

* `build` job
    * `$DIST-*.tar.gz`
* `test` job
    * `tap.tar.gz`, the archive of raw TAP from the tests, via
        `TAP::Harness::Archive`
    * `junit.xml`, the TAP results converted to JUnit

The `test` job consumes the built tarball from the `build` job.  `junit.xml`
is fed back to GitLab, marked explicitly as a [`artifacts:reports:junit`
report](https://docs.gitlab.com/ee/ci/yaml/#artifactsreportsjunit).

# Pipeline configuration of _this_ repository

The pipeline configuration of this repository does a couple things:

1. Builds the images
1. Runs a test "build" job using the new images and a scratch distribution
1. Runs a test "test" job using the new images and a scratch distribution
1. Promotes the images to "real" when the prior stages pass

To do this, we include the pipeline configuration we provide to consumers.
